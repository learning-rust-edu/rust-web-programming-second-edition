use crate::counter::Counter;
use actix_cors::Cors;
use actix_service::Service;
use actix_web::middleware::Logger;
use actix_web::{App, HttpResponse, HttpServer};
use futures::future::{ok, Either};

#[macro_use]
extern crate diesel;
extern crate dotenv;
mod config;
mod counter;
mod database;
mod json_serialization;
mod jwt;
mod models;
mod schema;
mod to_do;
mod views;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    const ALLOWED_VERSION: &str = include_str!("./output_data.txt");

    env_logger::init_from_env(env_logger::Env::new().default_filter_or("info"));

    let site_counter = Counter { count: 0 };
    site_counter.save().ok();

    HttpServer::new(|| {
        let cors = Cors::default()
            .allow_any_origin()
            .allow_any_method()
            .allow_any_header();
        App::new()
            .wrap(cors)
            .wrap_fn(|req, srv| {
                let passed = req.path().contains(&format!("/{}/", ALLOWED_VERSION));
                let mut site_counter = Counter::load().unwrap();
                site_counter.count += 1;
                println!("{:?}", &site_counter);
                site_counter.save().ok();
                let end_result = if passed {
                    Either::Left(srv.call(req))
                } else {
                    let resp = HttpResponse::NotImplemented()
                        .body(format!("only {} API is supported", ALLOWED_VERSION));
                    Either::Right(ok(req.into_response(resp).map_into_right_body()))
                };
                async move {
                    let result = end_result.await?;
                    Ok(result)
                }
            })
            .configure(views::views_factory)
            .wrap(Logger::new("%a %{User-Agent}i %r %s %D"))
    })
    .bind(("127.0.0.1", 8080))?
    .run()
    .await
}
