use crate::json_serialization::to_do_items::ToDoItems;
use crate::jwt::JwtToken;
use actix_web::Responder;

pub async fn get_all(token: JwtToken) -> impl Responder {
    ToDoItems::get_state(token.user_id)
}
