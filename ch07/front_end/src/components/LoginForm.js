import { useState } from 'react';
import axios from 'axios';
import './LoginForm.css';

const LoginForm = ({ handleLogin }) => {
  const [ username, setUsername ] = useState('');
  const [ password, setPassword ] = useState('');

  const handlePasswordChange = event => {
    setPassword(event.target.value);
  };

  const handleUsernameChange = event => {
    setUsername(event.target.value);
  };

  const resetForm = () => {
    setPassword('');
    setUsername('');
  };

  const submitLogin = event => {
    event.preventDefault();
    axios.post('http://127.0.0.1:8080/v1/auth/login',
      {
        'username': username,
        'password': password
      }, {
        headers: {
          'Access-Control-Allow-Origin': '*'
        }
      }
    ).then(response => {
      resetForm();
      handleLogin(response.data['token']);
    }).catch(error => {
      alert(error);
      resetForm();
    });
  };

  return (
    <form
      className='login'
      onSubmit={submitLogin}
    >
      <h1 className='login-title'>Login</h1>
      <input
        type='text'
        className='login-input'
        placeholder='Username'
        autoFocus
        onChange={handleUsernameChange}
        value={username}
      />
      <input
        type='password'
        className='login-input'
        placeholder='Password'
        onChange={handlePasswordChange}
        value={password}
      />
      <input
        type='submit'
        value='Lets Go'
        className='login-button'
      />
    </form>
  );
};

export {
  LoginForm,
};
