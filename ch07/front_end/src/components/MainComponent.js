import { useEffect, useState } from 'react';
import axios from 'axios';
import { ListItems } from './ListItems';
import { CreateToDoItem } from './CreateToDoItem';
import { LoginForm } from './LoginForm';

const USER_TOKEN_KEY = 'user-token';

const MainComponent = () => {
  const [ updated, setUpdated ] = useState(true);
  const [ doneItems, setDoneItems ] = useState([]);
  const [ pendingItems, setPendingItems ] = useState([]);
  const [ doneItemsCount, setDoneItemsCount ] = useState(0);
  const [ pendingItemsCount, setPendingItemsCount ] = useState(0);
  const [ loginStatus, setLoginStatus ] = useState(false);

  useEffect(() => {
    if (updated) {
      let token = localStorage.getItem(USER_TOKEN_KEY);
      if (token) {
        setLoginStatus(true);
        getItems();
      }
    }
  }, [ updated ]);

  const getItems = () => {
    const config = {
      headers: {
        token: localStorage.getItem(USER_TOKEN_KEY),
      }
    };
    axios.get('http://127.0.0.1:8080/v1/item/get', config)
    .then(response => {
      if (response.data) {
        setUpdated(false);
        setDoneItems(response.data['done_items']);
        setPendingItems(response.data['pending_items']);
        setDoneItemsCount(response.data['done_item_count']);
        setPendingItemsCount(response.data['pending_item_count']);
      }
    });
  };

  const handleLogin = token => {
    localStorage.setItem(USER_TOKEN_KEY, token);
    setLoginStatus(true);
    getItems();
  };

  const logout = () => {
    localStorage.removeItem(USER_TOKEN_KEY);
    setLoginStatus(false);
  };

  const sendRequest = (title, status) => {
    const action = status === 'PENDING' ? 'edit' : 'delete';
    const newStatus = status === 'PENDING' ? 'DONE' : 'PENDING';
    axios.post(`http://127.0.0.1:8080/v1/item/${action}`, {
      'title': title,
      'status': newStatus,
    }, {
      headers: {
        token: localStorage.getItem(USER_TOKEN_KEY),
      }
    }).then(response => {
      if (response.data) {
        setUpdated(true);
      }
    }).catch(error => {
      if (error.response.status === 401) {
        logout();
      }
    });
  };

  return <div className='mainContainer'>
    {loginStatus ?
      <>
        <div className='header'>
          <p>complete tasks: {doneItemsCount}</p>
          <p>pending tasks: {pendingItemsCount}</p>
          <div className='actionButton' onClick={() => logout()}>Logout</div>
        </div>
        <h1>Done Items</h1>
        <ListItems
          items={doneItems}
          sendRequest={sendRequest}
          logout={logout}
        />
        <h1>Pending Items</h1>
        <ListItems
          items={pendingItems}
          sendRequest={sendRequest}
          logout={logout}
        />
        <CreateToDoItem setUpdated={setUpdated} />
      </> :
      <>
        <LoginForm handleLogin={handleLogin} />
      </>
    }
  </div>;
};

export {
  MainComponent,
};
