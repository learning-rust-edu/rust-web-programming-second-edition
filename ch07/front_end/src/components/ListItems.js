import { ToDoItem } from './ToDoItem';

const ListItems = ({ items, sendRequest }) => (
  <>
    {items &&
      <>
        {items.map(item => (
          <ToDoItem
            key={item.title + item.status}
            title={item.title}
            status={item.status}
            sendRequest={sendRequest}
          />
        ))}
      </>
    }
  </>
);

export {
  ListItems,
};
