import { useEffect, useState } from 'react';
import axios from 'axios';
import { ListItems } from './ListItems';
import { CreateToDoItem } from './CreateToDoItem';

const MainComponent = () => {
  const [ updated, setUpdated ] = useState(true);
  const [ doneItems, setDoneItems ] = useState([]);
  const [ pendingItems, setPendingItems ] = useState([]);
  const [ doneItemsCount, setDoneItemsCount ] = useState(0);
  const [ pendingItemsCount, setPendingItemsCount ] = useState(0);

  useEffect(() => {
    if (updated) {
      const config = {
        headers: {
          token: 'secret token',
        }
      };
      axios.get('http://127.0.0.1:8080/v1/item/get', config)
      .then(response => {
        if (response.data) {
          setUpdated(false);
          setDoneItems(response.data['done_items']);
          setPendingItems(response.data['pending_items']);
          setDoneItemsCount(response.data['done_item_count']);
          setPendingItemsCount(response.data['pending_item_count']);
        }
      });
    }
  }, [ updated ]);

  const sendRequest = (title, status) => {
    const action = status === 'PENDING' ? 'edit' : 'delete';
    const newStatus = status === 'PENDING' ? 'DONE' : 'PENDING';
    axios.post(`http://127.0.0.1:8080/v1/item/${action}`, {
      'title': title,
      'status': newStatus,
    }, {
      headers: {
        token: 'secret-token',
      }
    }).then(response => {
      if (response.data) {
        setUpdated(true);
      }
    });
  };

  return <div className='mainContainer'>
    <div className='header'>
      <p>complete tasks: {doneItemsCount}</p>
      <p>pending tasks: {pendingItemsCount}</p>
    </div>
    <h1>Done Items</h1>
    <ListItems
      items={doneItems}
      sendRequest={sendRequest}
    />
    <h1>Pending Items</h1>
    <ListItems
      items={pendingItems}
      sendRequest={sendRequest}
    />
    <CreateToDoItem setUpdated={setUpdated} />
  </div>;
};

export {
  MainComponent,
};
