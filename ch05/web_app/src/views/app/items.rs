use crate::views::app::content_loader;
use crate::views::app::content_loader::add_component;
use actix_web::HttpResponse;

pub async fn items() -> HttpResponse {
    let mut html_data = content_loader::read_file("./templates/main.html");
    let javascript_data = content_loader::read_file("./templates/javascript/main.js");
    let main_css = content_loader::read_file("./templates/css/main.css");
    let base_css = content_loader::read_file("./templates/css/base.css");
    html_data = html_data
        .replace("{{JAVASCRIPT}}", &javascript_data)
        .replace("{{BASE_CSS}}", &base_css)
        .replace("{{CSS}}", &main_css);
    html_data = add_component(String::from("header"), html_data);
    HttpResponse::Ok()
        .content_type("text/html; charset=utf-8")
        .body(html_data)
}
