use actix_web::web;

mod create;
mod delete;
mod edit;
mod get;

pub fn to_to_views_factory(app: &mut web::ServiceConfig) {
    app.service(
        web::scope("v1/item")
            .route("get", web::get().to(get::get_all))
            .route("create/{title}", web::post().to(create::create))
            .route("edit", web::post().to(edit::edit))
            .route("delete", web::delete().to(delete::delete))
            .route("delete", web::post().to(delete::delete)),
    );
}
