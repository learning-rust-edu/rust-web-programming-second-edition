use actix_web::web::{self, post, scope};

mod create;

pub fn user_views_factory(app: &mut web::ServiceConfig) {
    app.service(scope("v1/user").route("create", post().to(create::create)));
}
