use crate::database::DB;
use crate::json_serialization::to_do_items::ToDoItems;
use crate::jwt::JwtToken;
use crate::models::items::item::Item;
use crate::models::items::new_item::NewItem;
use crate::schema::to_do;
use actix_web::{HttpRequest, HttpResponse};
use diesel::prelude::*;

pub async fn create(token: JwtToken, req: HttpRequest, db: DB) -> HttpResponse {
    let title = req.match_info().get("title").unwrap().to_string();
    let mut connection = db.connection;
    let items = to_do::table
        .filter(to_do::columns::title.eq(&title.as_str()))
        .filter(to_do::columns::user_id.eq(token.user_id))
        .order(to_do::columns::id.asc())
        .load::<Item>(&mut connection)
        .unwrap();
    if items.is_empty() {
        let new_item = NewItem::new(title, token.user_id);
        diesel::insert_into(to_do::table)
            .values(&new_item)
            .execute(&mut connection)
            .ok();
    }
    HttpResponse::Ok().json(ToDoItems::get_state(token.user_id))
}
