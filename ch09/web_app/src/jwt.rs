use crate::config::Config;
use actix_web::dev::Payload;
use actix_web::error::ErrorUnauthorized;
use actix_web::{Error, FromRequest, HttpRequest};
use chrono::Utc;
use futures::future::{err, ok, Ready};
use jsonwebtoken::{decode, encode, DecodingKey, EncodingKey, Header, Validation};
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct JwtToken {
    pub user_id: i32,
    pub exp: usize,
}

impl JwtToken {
    pub fn new(user_id: i32) -> Self {
        let config = Config::new();
        let minutes = config
            .map
            .get("EXPIRE_MINUTES")
            .unwrap_or(&serde_yaml::Value::default())
            .as_i64()
            .unwrap_or(60);
        let expiration = Utc::now()
            .checked_add_signed(chrono::Duration::minutes(minutes))
            .expect("invalid timestamp")
            .timestamp();
        JwtToken {
            user_id,
            exp: expiration as usize,
        }
    }

    pub fn from_token(token: String) -> Result<Self, String> {
        let key = DecodingKey::from_secret(JwtToken::get_key().as_ref());
        let token_result = decode::<JwtToken>(&token, &key, &Validation::default());
        match token_result {
            Ok(data) => Ok(data.claims),
            Err(error) => Err(format!("{}", error)),
        }
    }

    fn get_key() -> String {
        let config = Config::new();
        let key_str = config.map.get("SECRET_KEY").unwrap().as_str().unwrap();
        key_str.to_owned()
    }

    pub fn encode(self) -> String {
        let key = EncodingKey::from_secret(JwtToken::get_key().as_ref());
        encode(&Header::default(), &self, &key).unwrap()
    }
}

impl FromRequest for JwtToken {
    type Error = Error;
    type Future = Ready<Result<JwtToken, Error>>;

    fn from_request(req: &HttpRequest, _: &mut Payload) -> Self::Future {
        match req.headers().get("token") {
            Some(data) => {
                let raw_token = data.to_str().unwrap().to_string();
                let token_result = JwtToken::from_token(raw_token);
                match token_result {
                    Ok(token) => ok(token),
                    Err(message) => {
                        if message == "ExpiredSignature" {
                            err(ErrorUnauthorized("token expired"))
                        } else {
                            err(ErrorUnauthorized("token can't be decoded"))
                        }
                    }
                }
            }
            None => err(ErrorUnauthorized("token not in header under key 'token'")),
        }
    }
}

#[cfg(test)]
mod jwt_tests {
    use super::*;
    use actix_web::http::header::{ContentType, HeaderName, HeaderValue};
    use actix_web::test::{call_and_read_body_json, call_service, init_service, TestRequest};
    use actix_web::{web, App, HttpResponse};
    use serde_json::json;
    use std::str::FromStr;

    #[derive(Serialize, Deserialize, Debug)]
    pub struct ResponseFromTest {
        pub user_id: i32,
        pub exp_minutes: i32,
    }

    #[test]
    fn get_key() {
        assert_eq!(String::from("secret"), JwtToken::get_key());
    }

    #[test]
    fn get_exp() {
        let config = Config::new();
        let minutes = config.map.get("EXPIRE_MINUTES").unwrap().as_i64().unwrap();
        assert_eq!(120, minutes);
    }

    #[test]
    fn decode_incorrect_token() {
        let encoded_token: String = String::from("invalid_token");
        match JwtToken::from_token(encoded_token) {
            Err(message) => assert_eq!("InvalidToken", message),
            _ => panic!("Incorrect token should not be able to be encoded"),
        }
    }

    #[test]
    fn encode_decode() {
        let number = 5;
        let test_token = JwtToken::new(number);
        let encoded_token = test_token.encode();

        let new_token = JwtToken::from_token(encoded_token).unwrap();

        assert_eq!(number, new_token.user_id);
    }

    async fn test_handler(token: JwtToken, _: HttpRequest) -> HttpResponse {
        HttpResponse::Ok().json(json!({
            "user_id": token.user_id,
            "exp_minutes": 60,
        }))
    }

    #[actix_web::test]
    async fn test_no_token_request() {
        let app = init_service(App::new().route("/", web::get().to(test_handler))).await;
        let req = TestRequest::default()
            .insert_header(ContentType::plaintext())
            .to_request();

        let resp = call_service(&app, req).await;

        assert_eq!("401", resp.status().as_str());
    }

    #[actix_web::test]
    async fn test_passing_token_request() {
        let user_id = 5;
        let encoded_token = JwtToken::new(user_id).encode();
        let app = init_service(App::new().route("/", web::get().to(test_handler))).await;
        let mut req = TestRequest::default()
            .insert_header(ContentType::plaintext())
            .to_request();
        let header_name = HeaderName::from_str("token").unwrap();
        let header_value = HeaderValue::from_str(encoded_token.as_str()).unwrap();
        req.headers_mut().insert(header_name, header_value);

        let resp: ResponseFromTest = call_and_read_body_json(&app, req).await;

        assert_eq!(user_id, resp.user_id);
    }

    #[actix_web::test]
    async fn test_false_token_request() {
        let app = init_service(App::new().route("/", web::get().to(test_handler))).await;
        let mut req = TestRequest::default()
            .insert_header(ContentType::plaintext())
            .to_request();
        let header_name = HeaderName::from_str("token").unwrap();
        let header_value = HeaderValue::from_str("test").unwrap();
        req.headers_mut().insert(header_name, header_value);

        let resp = call_service(&app, req).await;

        assert_eq!("401", resp.status().as_str());
    }
}
