use crate::to_do::enums::TaskStatus;
use crate::to_do::structs::base::Base;

pub struct Done {
    pub super_struct: Base,
}

impl Done {
    pub fn new(input_title: &str) -> Self {
        let base = Base {
            title: input_title.to_string(),
            status: TaskStatus::Done,
        };
        Done { super_struct: base }
    }
}

#[cfg(test)]
mod done_tests {
    use super::*;

    #[test]
    fn test_new() {
        let expected_title = String::from("test title");

        let done = Done::new(&expected_title.clone());

        assert_eq!(expected_title, done.super_struct.title);
        assert_eq!(TaskStatus::Done, done.super_struct.status);
    }
}
