# Rust Web Programming - Second Edition

Sample code for "Rust Web Programming - Second Edition" book from Packt Publishing.

## License
[MIT](./LICENSE)
