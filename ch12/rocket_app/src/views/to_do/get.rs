use crate::json_serialization::to_do_items::ToDoItems;
use crate::jwt::JwtToken;
use rocket::serde::json::Json;

#[get("/get")]
pub async fn get_all(token: JwtToken) -> Json<ToDoItems> {
    Json(ToDoItems::get_state(token.user_id))
}
