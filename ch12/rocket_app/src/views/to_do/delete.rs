use crate::database::DB;
use crate::json_serialization::to_do_item::ToDoItem;
use crate::json_serialization::to_do_items::ToDoItems;
use crate::jwt::JwtToken;
use crate::models::items::item::Item;
use crate::schema::to_do;
use diesel::prelude::*;
use rocket::serde::json::Json;

#[post("/delete", data = "<to_do_item>", format = "json")]
pub async fn delete(to_do_item: Json<ToDoItem>, token: JwtToken, db: DB) -> Json<ToDoItems> {
    let mut connection = db.connection;
    let results = to_do::table
        .filter(to_do::columns::title.eq(&to_do_item.title))
        .filter(to_do::columns::user_id.eq(&token.user_id))
        .order(to_do::columns::id.asc())
        .load::<Item>(&mut connection)
        .unwrap();
    diesel::delete(&results[0]).execute(&mut connection).ok();
    Json(ToDoItems::get_state(token.user_id))
}
