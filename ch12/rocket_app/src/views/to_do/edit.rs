use crate::database::DB;
use crate::json_serialization::to_do_item::ToDoItem;
use crate::json_serialization::to_do_items::ToDoItems;
use crate::jwt::JwtToken;
use crate::schema::to_do;
use crate::to_do::enums::TaskStatus;
use diesel::prelude::*;
use rocket::serde::json::Json;

#[post("/edit", data = "<to_do_item>", format = "json")]
pub async fn edit(to_do_item: Json<ToDoItem>, token: JwtToken, db: DB) -> Json<ToDoItems> {
    let mut connection = db.connection;
    let results = to_do::table
        .filter(to_do::columns::title.eq(&to_do_item.title))
        .filter(to_do::columns::user_id.eq(&token.user_id));
    diesel::update(results)
        .set(to_do::columns::status.eq(TaskStatus::Done.stringify()))
        .execute(&mut connection)
        .ok();
    Json(ToDoItems::get_state(token.user_id))
}
