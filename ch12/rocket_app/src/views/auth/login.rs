use crate::database::DB;
use crate::json_serialization::login::Login;
use crate::json_serialization::login_response::LoginResponse;
use crate::jwt::JwtToken;
use crate::models::users::user::User;
use crate::schema::users;
use diesel::prelude::*;
use rocket::serde::json::Json;

#[post("/login", data = "<credentials>", format = "json")]
pub async fn login(credentials: Json<Login>, db: DB) -> Json<LoginResponse> {
    let mut db_connection = db.connection;
    let username = credentials.username.clone();
    let password = credentials.password.clone();
    let users = users::table
        .filter(users::columns::username.eq(username))
        .load::<User>(&mut db_connection)
        .unwrap();
    if users.is_empty() {
        panic!("no user found");
    } else if users.len() > 1 {
        panic!("more than one user found");
    }
    match users[0].clone().verify(password) {
        true => {
            let user_id = users[0].clone().id;
            let token = JwtToken::new(user_id);
            let raw_token = token.encode();
            let body = LoginResponse { token: raw_token };
            Json(body)
        }
        false => panic!("unauthorized"),
    }
}
