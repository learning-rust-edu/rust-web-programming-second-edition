use serde::{Serialize, Serializer};

#[derive(PartialEq, Debug, Clone)]
pub enum TaskStatus {
    Done,
    Pending,
}

impl TaskStatus {
    pub fn stringify(&self) -> String {
        match self {
            Self::Done => "DONE".to_string(),
            Self::Pending => "PENDING".to_string(),
        }
    }

    pub fn from_string(input_string: &str) -> Self {
        match input_string.to_uppercase().as_str() {
            "DONE" => TaskStatus::Done,
            "PENDING" => TaskStatus::Pending,
            _ => panic!("input {} not supported", input_string),
        }
    }
}

impl Serialize for TaskStatus {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(self.stringify().as_str())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_stringify() {
        let status = TaskStatus::Done;
        let result = status.stringify();
        assert_eq!(result, "DONE");

        let status = TaskStatus::Pending;
        let result = status.stringify();
        assert_eq!(result, "PENDING");
    }

    #[test]
    fn test_from_string() {
        let status = "Done"; // should ignore case
        let result = TaskStatus::from_string(&status);
        assert_eq!(result, TaskStatus::Done);

        let status = "pending"; // should ignore case
        let result = TaskStatus::from_string(&status);
        assert_eq!(result, TaskStatus::Pending);
    }
}
