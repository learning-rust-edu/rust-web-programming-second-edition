use crate::config::Config;
use diesel::r2d2::{ConnectionManager, Pool, PooledConnection};
use diesel::PgConnection;
use lazy_static::lazy_static;
use rocket::http::Status;
use rocket::request::{FromRequest, Outcome};
use rocket::Request;

#[derive(Debug)]
pub enum DBError {
    Unavailable,
}

type PgPool = Pool<ConnectionManager<PgConnection>>;
pub struct DbConnection {
    pub db_connection: PgPool,
}

lazy_static! {
    pub static ref DBCONNECTION: DbConnection = {
        let config = Config::new();
        let connection_string = config.map.get("DATABASE_URL").unwrap().as_str().unwrap();
        DbConnection {
            db_connection: PgPool::builder()
                .max_size(8)
                .build(ConnectionManager::new(connection_string))
                .expect("failed to create db connection_pool"),
        }
    };
}

pub struct DB {
    pub connection: PooledConnection<ConnectionManager<PgConnection>>,
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for DB {
    type Error = DBError;

    async fn from_request(_: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        match DBCONNECTION.db_connection.get() {
            Ok(connection) => Outcome::Success(DB { connection }),
            Err(_) => Outcome::Failure((Status::BadRequest, DBError::Unavailable)),
        }
    }
}
