use actix_web::web::ServiceConfig;

mod auth;

pub fn views_factory(app: &mut ServiceConfig) {
    auth::auth_views_factory(app);
}
