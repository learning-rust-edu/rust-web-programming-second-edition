use std::sync::{Arc, Mutex};
use std::{thread, time};
use futures::executor::block_on;
use futures::future::join_all;
use futures::join;

async fn do_something(number: i8) -> i8 {
    println!("number {} is running", number);
    let two_seconds = time::Duration::new(2, 0);
    thread::sleep(two_seconds);
    2
}

fn main() {
    let now = time::Instant::now();
    let future_one = do_something(1);
    let outcome = block_on(future_one);
    let future_two = async {
        let outcome_one = do_something(2).await;
        let outcome_two = do_something(3).await;
        outcome_one + outcome_two
    };
    let future_two = block_on(future_two);
    println!("Here is the outcome: {:?}", future_two);
    println!("time elapsed {:?}", now.elapsed());
    println!("Here is the outcome: {}", outcome);

    let future_four = async {
        let outcome_one = do_something(4);
        let outcome_two = do_something(5);
        let results = join!(outcome_one, outcome_two);
        results.0 + results.1
    };
    let now = time::Instant::now();
    let result = block_on(future_four);
    println!("time elapsed {:?}", now.elapsed()); // still takes 4 seconds to execute
    println!("here is the result: {:?}", result);

    // async_std sample
    let async_outcome = async {
        // 1.
        let mut futures_vec = Vec::new();
        let future_six = do_something(6);
        let future_seven = do_something(7);
        // 2.
        futures_vec.push(future_six);
        futures_vec.push(future_seven);
        // 3.
        let handles = futures_vec
            .into_iter()
            .map(async_std::task::spawn)
            .collect::<Vec<_>>();
        // 4.
        let results = join_all(handles).await;
        results.into_iter().sum::<i8>()
    };
    let now = time::Instant::now();
    let result = block_on(async_outcome);
    println!("time elapsed for join vec {:?}", now.elapsed()); // this is executed in 2 seconds
    println!("Here is the result: {:?}", result);
    println!();

    // passing variables into threads and async tasks is not straightforward
    // std::sync::Arc: This type enables threads to reference outside data
    let names = Arc::new(vec!["dave", "chloe", "simon"]);
    let reference_data = Arc::clone(&names);
    let new_thread = thread::spawn(move || {
        println!("{}", reference_data[1]);
    });
    // names can be used after thread spawn
    println!("{:?}", names);
    // reference_data cannot be used, because it was borrowed with move in spawn
    // next line will not compile
    // println!("{:?}", reference_data);
    new_thread.join().unwrap();

    // std::sync::Mutex: This type enables threads to mutate outside data
    let count = Mutex::new(0);
    println!("count = {:?}", count);
    thread::spawn(move || {
        *count.lock().unwrap() += 1;
        println!("count = {:?}", count);
    })
    .join()
    .unwrap();
}
