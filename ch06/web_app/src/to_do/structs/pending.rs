use crate::to_do::enums::TaskStatus;
use crate::to_do::structs::base::Base;

pub struct Pending {
    pub super_struct: Base,
}

impl Pending {
    pub fn new(input_title: &str) -> Self {
        let base = Base {
            title: input_title.to_string(),
            status: TaskStatus::Pending,
        };
        Pending { super_struct: base }
    }
}
