use crate::database::establish_connection;
use crate::json_serialization::to_do_item::ToDoItem;
use crate::json_serialization::to_do_items::ToDoItems;
use crate::jwt::JwtToken;
use crate::models::items::item::Item;
use crate::schema::to_do;
use actix_web::{web, HttpResponse};
use diesel::prelude::*;

pub async fn delete(to_do_item: web::Json<ToDoItem>, _token: JwtToken) -> HttpResponse {
    let mut connection = establish_connection();
    let results = to_do::table
        .filter(to_do::columns::title.eq(&to_do_item.title))
        .order(to_do::columns::id.asc())
        .load::<Item>(&mut connection)
        .unwrap();
    diesel::delete(&results[0]).execute(&mut connection).ok();
    HttpResponse::Ok().json(ToDoItems::get_state())
}
