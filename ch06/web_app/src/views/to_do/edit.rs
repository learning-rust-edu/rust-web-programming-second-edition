use crate::database::DB;
use crate::json_serialization::to_do_item::ToDoItem;
use crate::json_serialization::to_do_items::ToDoItems;
use crate::jwt::JwtToken;
use crate::schema::to_do;
use crate::to_do::enums::TaskStatus;
use actix_web::{web, HttpResponse};
use diesel::prelude::*;

pub async fn edit(to_do_item: web::Json<ToDoItem>, _token: JwtToken, db: DB) -> HttpResponse {
    let mut connection = db.connection;
    let results = to_do::table.filter(to_do::columns::title.eq(&to_do_item.title));
    diesel::update(results)
        .set(to_do::columns::status.eq(TaskStatus::Done.stringify()))
        .execute(&mut connection)
        .ok();
    HttpResponse::Ok().json(ToDoItems::get_state())
}
