use crate::constants::FILE_NAME;
use serde_json::{json, Map, Value};
use std::fs;
use std::fs::File;
use std::io::Read;

fn open_or_create(file_name: &str) -> std::io::Result<File> {
    fs::OpenOptions::new()
        .create(true)
        .read(true)
        .write(true)
        .open(file_name)
}

pub fn load_state() -> Map<String, Value> {
    read_file(FILE_NAME)
}

fn read_file(file_name: &str) -> Map<String, Value> {
    let mut file = open_or_create(file_name).unwrap();
    let mut data = String::new();
    file.read_to_string(&mut data).unwrap();
    let json: Value = serde_json::from_str(&data).unwrap_or(Value::default());
    let state: Map<String, Value> = json.as_object().unwrap_or(&Map::new()).clone();
    state
}

pub fn save_state(state: &mut Map<String, Value>) {
    write_to_file(FILE_NAME, state);
}

fn write_to_file(file_name: &str, state: &mut Map<String, Value>) {
    let new_data = json!(state);
    fs::write(file_name, new_data.to_string()).expect("Unable to write file");
}
