use crate::processes::process_input;
use crate::state::load_state;
use crate::to_do::enums::TaskStatus;
use crate::to_do::to_do_factory;
use actix_web::HttpRequest;

pub async fn create(req: HttpRequest) -> String {
    let state = load_state();
    let title = req.match_info().get("title").unwrap().to_string();
    let item = to_do_factory(title.as_str(), TaskStatus::Pending);
    process_input(item, "create".to_string(), &state);
    format!("{} created", title)
}
