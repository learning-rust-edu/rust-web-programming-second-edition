use std::collections::HashMap;
use tokio::sync::{mpsc, oneshot};

#[derive(Debug, Clone)]
pub enum TrackerOrder {
    Buy(String, f32),
    Get,
}

#[derive(Debug)]
pub struct TrackerMessage {
    pub command: TrackerOrder,
    pub respond_to: oneshot::Sender<String>,
}

#[derive(Debug)]
pub struct GetTrackerActor {
    pub sender: mpsc::Sender<TrackerMessage>,
}

impl GetTrackerActor {
    pub async fn send(self) -> String {
        println!("GET function firing");
        let (send, recv) = oneshot::channel();
        let message = TrackerMessage {
            command: TrackerOrder::Get,
            respond_to: send,
        };
        let _ = self.sender.send(message).await;
        match recv.await {
            Ok(outcome) => outcome,
            Err(e) => panic!("{}", e),
        }
    }
}

pub struct TrackerActor {
    pub receiver: mpsc::Receiver<TrackerMessage>,
    pub db: HashMap<String, f32>,
}

impl TrackerActor {
    pub fn new(receiver: mpsc::Receiver<TrackerMessage>) -> Self {
        TrackerActor {
            receiver,
            db: HashMap::new(),
        }
    }

    fn send_state(&self, respond_to: oneshot::Sender<String>) {
        let mut buffer = vec![];
        for key in self.db.keys() {
            let amount = self.db.get(key).unwrap();
            buffer.push(format!("{}:{ };", &key, amount));
        }
        buffer.push("\n".to_string());
        println!("sending state: {}", buffer.join(""));
        let _ = respond_to.send(buffer.join(""));
    }

    fn handle_message(&mut self, message: TrackerMessage) {
        match message.command {
            TrackerOrder::Buy(ticker, amount) => match self.db.get(&ticker) {
                Some(ticker_amount) => {
                    self.db.insert(ticker, ticker_amount + amount);
                }
                None => {
                    self.db.insert(ticker, amount);
                }
            },
            TrackerOrder::Get => {
                println!("getting state");
                self.send_state(message.respond_to);
            }
        }
    }

    pub async fn run(mut self) {
        println!("tracker actor is running");
        while let Some(msg) = self.receiver.recv().await {
            self.handle_message(msg);
        }
    }
}
