use crate::tasks::add::AddTask;
use crate::tasks::multiply::MultiplyTask;
use crate::tasks::subtract::SubtractTask;
use serde::{Deserialize, Serialize};

pub mod add;
pub mod multiply;
pub mod subtract;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum TaskType {
    Add(AddTask),
    Multiply(MultiplyTask),
    Subtract(SubtractTask),
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TaskMessage {
    pub task: TaskType,
}
